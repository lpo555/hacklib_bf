#include "BattlefieldDataMgr.h"
#include "main.h"
#include "hacklib/PatternScanner.h"

using std::uintptr_t;


static BattlefieldDataMgr *g_this = nullptr;
static std::mutex g_mutexHook;
static const char moduleNameBFBC2[] = "BFBC2Game.exe";
static const char moduleNameBF3[] = "bf3.exe";
static const char moduleNameBF4[] = "bf4.exe";


void __fastcall hkBFBC2InputUpdate(BFBC2::BorderInputNode *pInst, int, float deltaTime);
void __fastcall hkBF3InputUpdate(BF3::BorderInputNode *pInst, int, float deltaTime);
void __fastcall hkBF4InputUpdate(BF4::IMouse *pInst, int, float deltaTime);


BattlefieldDataMgr::BattlefieldDataMgr()
{
    g_this = this;

    m_hkBFBC2InputUpdate = nullptr;
    m_hkBF3InputUpdate = nullptr;
    m_hkBF4InputUpdate = nullptr;

    m_overwriteInput = false;
    m_inputYaw = 0;
    m_inputPitch = 0;
    m_camYawDiff = 0;
    m_camPitchDiff = 0;

    m_version = VERSION_UNKNOWN;
}

BattlefieldDataMgr *BattlefieldDataMgr::getInstance()
{
    return g_this;
}


bool BattlefieldDataMgr::init()
{
    // get module base
    HMODULE hMod = GetModuleHandle(moduleNameBFBC2);
    if (hMod) {
        m_version = VERSION_BFBC2;
    } else {
        hMod = GetModuleHandle(moduleNameBF3);
        if (hMod) {
            m_version = VERSION_BF3;
        } else {
            hMod = GetModuleHandle(moduleNameBF4);
            if (hMod) {
                m_version = VERSION_BF4;
            } else {
                LOG_ERR("could not find any battlefield module\n");
                return false;
            }
        }
    }

    switch(m_version)
    {
    case VERSION_BFBC2:
        LOG_INFO("# MODE: BFBC2\n");
        return InitBFBC2();
    case VERSION_BF3:
        LOG_INFO("# MODE: BF3\n");
        return InitBF3();
    case VERSION_BF4:
        LOG_INFO("# MODE: BF4\n");
        return InitBF4();
    }
    return false;
}

void BattlefieldDataMgr::safeUnhook()
{
    if (m_hkBFBC2InputUpdate)
        m_hooker.unhookVT(m_hkBFBC2InputUpdate);
    if (m_hkBF3InputUpdate)
        m_hooker.unhookVT(m_hkBF3InputUpdate);
    if (m_hkBF4InputUpdate)
        m_hooker.unhookVT(m_hkBF4InputUpdate);
    std::lock_guard<std::mutex> lock(g_mutexHook);
}

void BattlefieldDataMgr::update()
{
    m_data.m_internalRep = nullptr;
    __try {
        switch (m_version)
        {
        case VERSION_BFBC2:
            UpdateBFBC2();
            break;
        case VERSION_BF3:
            UpdateBF3();
            break;
        case VERSION_BF4:
            UpdateBF4();
            break;
        }
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        LOG_DBG("exception on update\n");
        m_data.m_internalRep = nullptr;
    }
}


const BattlefieldData *BattlefieldDataMgr::getData() const
{
    if (m_data.m_internalRep)
        return &m_data;
    return nullptr;
}


void BattlefieldDataMgr::setAimAngles(float yaw, float pitch)
{
    m_inputYaw = yaw;
    m_inputPitch = pitch;
    m_overwriteInput = true;
}

void BattlefieldDataMgr::setAimPos(const D3DXVECTOR3& pos)
{
    m_targetPos = pos;
}

bool BattlefieldDataMgr::hkInputUpdateCam(float *yaw, float *pitch)
{
    update();

    __try {
        gameCallback();
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        LOG_ERR("unexpected error in game thread\n");
    }

    if (m_overwriteInput) {
        auto cam = m_data.getCamData();
        if (cam)
        {
            float camYaw = atan2(cam->getViewVec().y, cam->getViewVec().x);
            float camPitch = atan2(cam->getViewVec().z, sqrt(cam->getViewVec().x*cam->getViewVec().x + cam->getViewVec().y*cam->getViewVec().y));
            //LOG_DBG("current cam angles: %f %f\n", camYaw, camPitch);
            //LOG_DBG("should be angles: %f %f\n", m_inputYaw, m_inputPitch);

            //*yaw = 0.039f * (m_inputYaw - camYaw);
            //*pitch = 0.039f * (m_inputPitch - camPitch);
            *yaw = (1 / cam->getFovY()) * 0.024f * (m_inputYaw - camYaw);
            *pitch = (1 / cam->getFovY()) *0.024f * (m_inputPitch - camPitch);
            //LOG_DBG("diff angles: %f %f\n", *yaw, *pitch);
            m_overwriteInput = false;
            return true;
        }
    }
    return false;
}


bool BattlefieldDataMgr::InitBFBC2()
{
    uintptr_t sigIngame = hl::FindPattern("\x83\xc4\x04\x8b\xce\xe8\x00\x00\x00\x00\x5b", "xxxxxx????x");
    uintptr_t sigCtx = hl::FindPattern("\xb8\x01\x00\x00\x00\x84\x05\x00\x00\x00\x00\x75\x66", "xxxxxxx????xx");
    uintptr_t sigCam = hl::FindPattern("\x8d\x47\x10\x81\xc3", "xxxxx");
    uintptr_t sigFovy = hl::FindPattern("\x39\x5e\x38\x74\x6f", "xxxxx");
    uintptr_t sigMystate = hl::FindPattern("\x8b\x48\x18\x8b\x11\x8d\x46\x2c", "xxxxxxxx");
    uintptr_t sigInput = hl::FindPattern("\x8b\x0d\x00\x00\x00\x00\x8b\x01\x8b\x50\x0c\xff\xd2\x8b\x0d", "xx????xxxxxxxxx");
    
    LOG_DBG("sigingame      %p\n", sigIngame);
    LOG_DBG("sigctx         %p\n", sigCtx);
    LOG_DBG("sigcam         %p\n", sigCam);
    LOG_DBG("sigfovy        %p\n", sigFovy);
    LOG_DBG("sigmystate     %p\n", sigMystate);
    LOG_DBG("siginput       %p\n", sigInput);

    if (!sigIngame || !sigCtx ||!sigCam || !sigFovy || !sigMystate || !sigInput) {
        LOG_DBG("one or more patterns were not found\n");
        return false;
    }

    __try {
        m_memsBFBC2.pIsIngame = *(unsigned char**)(hl::FollowRelativeAddress(sigIngame - 4) + 5);
        m_memsBFBC2.pCtx = *(BFBC2::ClientGameContext**)(sigCtx + 0x15);
        m_memsBFBC2.pCam = (const BFBC2::Cam**)(*(uintptr_t*)(sigCam + 0x5) + 0x30);
        m_memsBFBC2.fovy = *(uintptr_t*)(sigFovy + 0x7);
        m_memsBFBC2.pMystate = *(const BFBC2::Mystate***)(sigMystate - 0x9);
        m_memsBFBC2.pInput = *(const BFBC2::BorderInputNode***)(sigInput + 0x2);
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        LOG_DBG("one or more patterns are invalid\n");
        return false;
    }

    if (!m_memsBFBC2.pIsIngame || !m_memsBFBC2.pCtx || !m_memsBFBC2.pCam || !m_memsBFBC2.fovy || !m_memsBFBC2.pMystate || !m_memsBFBC2.pInput) {
        LOG_DBG("one or more patterns resulted in invalid mems\n");
        return false;
    }

    LOG_DBG("---------------\n");
    LOG_DBG("memisingame    %p\n", m_memsBFBC2.pIsIngame);
    LOG_DBG("memctx         %p\n", m_memsBFBC2.pCtx);
    LOG_DBG("memcam         %p\n", m_memsBFBC2.pCam);
    LOG_DBG("memfovy        %p\n", m_memsBFBC2.fovy);
    LOG_DBG("memmystate     %p\n", m_memsBFBC2.pMystate);
    LOG_DBG("meminput       %p\n", m_memsBFBC2.pInput);

    m_hkBFBC2InputUpdate = m_hooker.hookVT((uintptr_t)*m_memsBFBC2.pInput, 20, (uintptr_t)hkBFBC2InputUpdate);
    if (!m_hkBFBC2InputUpdate) {
        LOG_DBG("hooking input update failed\n");
        return false;
    }

    return true;
}

bool BattlefieldDataMgr::InitBF3()
{
    uintptr_t sigCtx = hl::FindPattern("\x83\xc7\x04\x3b\xfb\x75\xf0\x8b\x0d", "xxxxxxxxx");
    uintptr_t sigCam = hl::FindPattern("\x51\x8b\x0d\x00\x00\x00\x00\xd9\x1c\x24\x83\xc0\x10", "xxx????xxxxxx");
    uintptr_t sigInput = hl::FindPattern("\x8b\x35\x00\x00\x00\x00\x85\xf6\x74\x5c", "xx????xxxx");

    LOG_DBG("sigctx         %p\n", sigCtx);
    LOG_DBG("sigcam         %p\n", sigCam);
    LOG_DBG("siginput       %p\n", sigInput);

    if (!sigCtx || !sigCam || !sigInput) {
        LOG_DBG("one or more pattern were not found\n");
        return false;
    }

    __try {
        m_memsBF3.pCtx = **(BF3::ClientGameContext***)(sigCtx + 0x9);
        m_memsBF3.pCam = **(BF3::Cam***)(sigCam + 0x3);
        m_memsBF3.pInput = *(BF3::BorderInputNode**)(sigInput + 0x2);
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        LOG_DBG("one or more patterns are invalid\n");
        return false;
    }

    if (!m_memsBF3.pCtx || !m_memsBF3.pCam || !m_memsBF3.pInput) {
        LOG_DBG("one or more patterns resultet in invalid mems\n");
        return false;
    }

    LOG_DBG("---------------\n");
    LOG_DBG("memctx         %p\n", m_memsBF3.pCtx);
    LOG_DBG("memcam         %p\n", m_memsBF3.pCam);
    LOG_DBG("meminput       %p\n", m_memsBF3.pInput);

    // TODO hook input

    return true;
}

bool BattlefieldDataMgr::InitBF4()
{
    uintptr_t sigCtx = hl::FindPattern("\x45\x84\xf6\x75\x00\x84\xdb\x75\x00\x48\x8b\x05\x00\x00\x00\x00\x48\x8b\x48", "xxxx?xxx?xxx????xxx");
    uintptr_t sigCam = hl::FindPattern("\x84\xc0\x75\x00\x48\x8b\x0d\x00\x00\x00\x00\x48\x8b\x01\xff\x50\x00\xf3\x0f\x10\x0d", "xxx?xxx????xxxxx?xxxx");
    uintptr_t sigInput = hl::FindPattern("\x0f\x28\xc8\x0f\x29\x74\x24\x00\x0f\x28\xf0", "xxxxxxx?xxx");

    LOG_DBG("sigctx         %p\n", sigCtx);
    LOG_DBG("sigcam         %p\n", sigCam);
    LOG_DBG("siginput       %p\n", sigInput);

    if (!sigCtx || !sigCam  || !sigInput) {
        LOG_DBG("one or more pattern were not found\n");
        return false;
    }

    __try {
        auto ctxAdr = *(std::uint32_t*)(sigCtx + 0xc);
        m_memsBF4.pCtx = *(BF4::ClientGameContext**)(ctxAdr + sigCtx + 0xc + 0x4);
        LOG_DBG("ctxadr: %08X ctx: %p\n", ctxAdr, m_memsBF4.pCtx);

        auto camAdr = *(std::uint32_t*)(sigCam + 0x7);
        m_memsBF4.pCamHolder = *(BF4::CamHolder**)(camAdr + sigCam + 0x7 + 0x4);
        LOG_DBG("camadr: %08X cam: %p\n", camAdr, m_memsBF4.pCamHolder);

        auto inputAdr = *(std::uint32_t*)(sigInput - 0x4);
        m_memsBF4.pInput = *(BF4::BorderInputNode**)(inputAdr + sigInput - 0x4 + 0x4);
        LOG_DBG("inputadr: %08X input: %p\n", inputAdr, m_memsBF4.pInput);
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        LOG_DBG("one or more patterns are invalid\n");
        return false;
    }

    if (!m_memsBF4.pCtx || !m_memsBF4.pCamHolder || !m_memsBF4.pInput) {
        LOG_DBG("one or more patterns resultet in invalid mems\n");
        return false;
    }

    LOG_DBG("---------------\n");
    LOG_DBG("memctx         %p\n", m_memsBF4.pCtx);
    LOG_DBG("memcam         %p\n", m_memsBF4.pCamHolder);
    LOG_DBG("meminput       %p\n", m_memsBF4.pInput);

    m_hkBF4InputUpdate = m_hooker.hookVT((uintptr_t)m_memsBF4.pInput->getMouse(), 3, (uintptr_t)hkBF4InputUpdate);
    if (!m_hkBF4InputUpdate) {
        LOG_DBG("hooking input update failed\n");
        return false;
    }

    return true;
}



void BattlefieldDataMgr::UpdateBFBC2Skeleton(Skeleton *pSkel, const BFBC2::AnimatedSoldier *pData)
{
    pSkel->m_internalRep = nullptr;

    if (pData && 
        pData->getModelBase() && pData->getModelBase()->getWorldPose() &&
        pData->getSkeleton() && pData->getSkeleton()->getSkeleton() && pData->getSkeleton()->getSkeleton()->Bones)
    {
        pSkel->m_internalRep = (void*)pData->getSkeleton();
        pSkel->m_headIndex = -1;
        pSkel->m_chestIndex = -1;
        pSkel->m_hipsIndex = -1;

        D3DXMATRIX *worldTrans = pData->getModelBase()->getWorldPose()->first;
        auto skel = pData->getSkeleton()->getSkeleton();

        auto& bones = pSkel->m_bones;
        bones.resize(skel->BoneCount);
        for (int i = 0; i < skel->BoneCount; i++)
        {
            bool ignore = false;

            const char *name = skel->Bones[i].Name;
            if (strcmp(name, "grannyRootBone") == 0 || strcmp(name, "cameraBase") == 0)
                ignore = true;
            else if (strcmp(name, "Head") == 0)
                pSkel->m_headIndex = i;
            else if (strcmp(name, "Spine2") == 0)
                pSkel->m_chestIndex = i;
            else if (strcmp(name, "Hips") == 0)
                pSkel->m_hipsIndex = i;

            bones[i].ignore = ignore;
            bones[i].parent = skel->Bones[i].PartentIndex;
            bones[i].pos = D3DXVECTOR3(-worldTrans[i]._41, worldTrans[i]._43, worldTrans[i]._42);
        }
    }
}
void BattlefieldDataMgr::UpdateBFBC2Player(Player *pPlayer, const BFBC2::ClientPlayer *pData, bool isOwn)
{
    pPlayer->m_internalRep = nullptr;

    if (pData && pData->getSoldier().offsettedSoldier)
    {
        pPlayer->m_teamId = pData->getTeamId();
        pPlayer->m_name = std::string(pData->getName());

        BFBC2::ClientSoldierEntity *pSoldier = (BFBC2::ClientSoldierEntity*)(*pData->getSoldier().offsettedSoldier - 0x4);
        if (pSoldier)
        {
            pPlayer->m_internalRep = (void*)pData;

            if (pSoldier->getCollection()) {
                auto trans = *(BFBC2::fb::LinearTransform*)((uintptr_t)pSoldier->getCollection() + 0x10 + (pSoldier->getCollection()->offsetCount * 0x50));
                pPlayer->m_pos = D3DXVECTOR3(-trans.trans.x, trans.trans.z, trans.trans.y);
                pPlayer->m_rotation = atan2(trans.forward.z, -trans.forward.x);
            }

            BFBC2::fb::Vec3 speed = { 0, 0, 0 };
            if (pData->getAttachedControllable()) {
                speed = ((BFBC2::ClientVehicleEntity*)pData->getAttachedControllable())->getSpeed();
            } else if (pSoldier->getReplicatedController()) {
                speed = pSoldier->getReplicatedController()->getVelocity();
            }
            pPlayer->m_vel = D3DXVECTOR3(-speed.x, speed.z, speed.y);

            pPlayer->m_health = pSoldier->getHealth() / pSoldier->getMaxHealth();

            UpdateBFBC2Skeleton(&pPlayer->m_skeleton, pSoldier->getPersonView1().animation);

            // check distance of skeleton to soldier position
            if (pPlayer->getSkeleton())
            {
                auto hips = pPlayer->getSkeleton()->getHipsBone();
                if (hips)
                {
                    D3DXVECTOR3 dist = hips->pos - pPlayer->getPos();
                    if (D3DXVec3Length(&dist) > 2.0f)
                        pPlayer->m_skeleton.m_internalRep = nullptr;
                }
            }

            if (isOwn) {
                if (pSoldier->getCurrentWeapon() && pSoldier->getCurrentWeapon()->getData()) {
                    auto weap = pSoldier->getCurrentWeapon()->getData();

                    m_data.m_weapon.m_gravity = 9.81f;

                    if (weap->getWeaponFiring() && weap->getWeaponFiring()->getPrimaryFire()) {
                        m_data.m_weapon.m_bulletSpeed = weap->getWeaponFiring()->getPrimaryFire()->getShot().initialSpeed.z;
                        m_data.m_weapon.m_internalRep = (void*)(pSoldier->getCurrentWeapon());
                    }
                }
            }
        }
    }
}
void BattlefieldDataMgr::UpdateBFBC2()
{
    // is ingame
    m_data.m_isIngame = *m_memsBFBC2.pIsIngame == 0;

    // cam data
    if (*m_memsBFBC2.pCam) {
        D3DXVECTOR3 pos = (*m_memsBFBC2.pCam)->getPos();
        D3DXVECTOR3 viewVec = (*m_memsBFBC2.pCam)->getViewVec();
        m_data.m_camData.m_pos = D3DXVECTOR3(-pos.x, pos.z, pos.y);
        m_data.m_camData.m_viewVec = D3DXVECTOR3(-viewVec.x, viewVec.z, viewVec.y);
        m_data.m_camData.m_internalRep = (void*)(*m_memsBFBC2.pCam);
    } else {
        m_data.m_camData.m_internalRep = nullptr;
    }

    uintptr_t adrFovy = *(uintptr_t*)m_memsBFBC2.fovy;
    if (adrFovy) {
        float tryFovy = *(float*)(adrFovy + 0x30);
        if (tryFovy > 0.05f)
            m_data.m_camData.m_fovy = tryFovy;
    }

    // own data
    if (*m_memsBFBC2.pMystate) {
        m_data.m_ownData.m_reload = static_cast<float>((*m_memsBFBC2.pMystate)->getReload()) / 100.0f;
        m_data.m_ownData.m_overheat = (*m_memsBFBC2.pMystate)->getOverheat();
        m_data.m_ownData.m_spread = (*m_memsBFBC2.pMystate)->getSpread();
        m_data.m_ownData.m_internalRep = (void*)(*m_memsBFBC2.pMystate);
    } else {
        m_data.m_ownData.m_internalRep = nullptr;
    }

    m_data.m_weapon.m_internalRep = nullptr;

    // from context
    m_data.m_internalRep = (void*)m_memsBFBC2.pCtx;
    m_data.m_ownPlayerIndex = -1;
    auto ctx = m_memsBFBC2.pCtx;
    if (ctx->getPlayerMgr())
    {
        auto playerVec = ctx->getPlayerMgr()->getPlayers();

        m_data.m_players.resize(playerVec.size());
        for (size_t i = 0; i < playerVec.size(); i++) {
            bool isOwn = playerVec[i] == ctx->getPlayerMgr()->getLocalPlayer();
            UpdateBFBC2Player(&m_data.m_players[i], playerVec[i], isOwn);
            if (isOwn)
                m_data.m_ownPlayerIndex = i;
        }
    } else {
        m_data.m_players.clear();
    }
}
void __fastcall hkBFBC2InputUpdate(BFBC2::BorderInputNode *pInst, int, float deltaTime)
{
    std::lock_guard<std::mutex> lock(g_mutexHook);

    auto bfmgr = BattlefieldDataMgr::getInstance();

    uintptr_t orgFunc = bfmgr->m_hkBFBC2InputUpdate->getOrgFunc(20);
    ((void(__thiscall*)(BFBC2::BorderInputNode*, float))(orgFunc))(pInst, deltaTime);

    float yaw;
    float pitch;
    if (bfmgr->hkInputUpdateCam(&yaw, &pitch))
    {
        pInst->updateLevelInCache(BFBC2::InputConceptId::Yaw, -yaw);
        pInst->updateLevelInCache(BFBC2::InputConceptId::Pitch, pitch);
    }
}

void BattlefieldDataMgr::UpdateBF3Player(Player *pPlayer, const BF3::ClientPlayer *pData, bool isOwn)
{
    pPlayer->m_internalRep = nullptr;

    if (pData)
    {
        pPlayer->m_teamId = pData->getTeamId();
        pPlayer->m_name = std::string(pData->getName());

        auto pSoldier = pData->getSoldier();
        if (pSoldier)
        {
            pPlayer->m_internalRep = (void*)pData;

            auto trans = pSoldier->getMeshTransform();
            pPlayer->m_pos = D3DXVECTOR3(-trans.trans.x, trans.trans.z, trans.trans.y);
            pPlayer->m_rotation = atan2(trans.forward.z, -trans.forward.x);

            BF3::fb::Vec3 speed = { 0, 0, 0 };
            if (pData->getAttachedControllable()) {
                speed = ((BF3::ClientVehicleEntity*)pData->getAttachedControllable())->getSpeed();
            } else if (pSoldier->getReplicatedController()) {
                speed = pSoldier->getReplicatedController()->getVelocity();
            }
            pPlayer->m_vel = D3DXVECTOR3(-speed.x, speed.z, speed.y);

            pPlayer->m_health = pSoldier->getHealth() / 100.0f;

            // TODO skeleton
            pPlayer->m_skeleton.m_internalRep = nullptr;

            if (isOwn) {
                // TODO weapon
            }
        }
    }
}
void BattlefieldDataMgr::UpdateBF3()
{
    // is ingame
    m_data.m_isIngame = true;

    // cam data
    auto pCam = m_memsBF3.pCam;
    m_data.m_camData.m_pos = D3DXVECTOR3(-pCam->getPos().x, pCam->getPos().z, pCam->getPos().y);
    m_data.m_camData.m_viewVec = D3DXVECTOR3(pCam->getViewVec().x, -pCam->getViewVec().z, -pCam->getViewVec().y);
    m_data.m_camData.m_fovy = pCam->getFovy();
    m_data.m_camData.m_internalRep = (void*)pCam;

    // own data
    m_data.m_ownData.m_internalRep = nullptr;

    m_data.m_weapon.m_internalRep = nullptr;

    // form context
    m_data.m_internalRep = (void*)m_memsBF3.pCtx;
    m_data.m_ownPlayerIndex = -1;
    auto ctx = m_memsBF3.pCtx;
    if (ctx->getPlayerMgr())
    {
        auto playerVec = ctx->getPlayerMgr()->getPlayers();

        m_data.m_players.resize(playerVec.size());
        for (size_t i = 0; i < playerVec.size(); i++) {
            bool isOwn = playerVec[i] == ctx->getPlayerMgr()->getLocalPlayer();
            UpdateBF3Player(&m_data.m_players[i], playerVec[i], isOwn);
            if (isOwn)
                m_data.m_ownPlayerIndex = i;
        }
    } else {
        m_data.m_players.clear();
    }
}
void __fastcall hkBF3InputUpdate(BF3::BorderInputNode *pInst, int, float deltaTime)
{
    std::lock_guard<std::mutex> lock(g_mutexHook);

    auto bfmgr = BattlefieldDataMgr::getInstance();

    uintptr_t orgFunc = bfmgr->m_hkBFBC2InputUpdate->getOrgFunc(20);
    ((void(__thiscall*)(BF3::BorderInputNode*, float))(orgFunc))(pInst, deltaTime);

    float yaw;
    float pitch;
    if (bfmgr->hkInputUpdateCam(&yaw, &pitch))
    {
        //pInst->updateLevelInCache(BFBC2::InputConceptId::Yaw, -yaw);
        //pInst->updateLevelInCache(BFBC2::InputConceptId::Pitch, pitch);
    }
}

void BattlefieldDataMgr::UpdateBF4Player(Player *pPlayer, const BF4::ClientPlayer *pData, bool isOwn)
{
    pPlayer->m_internalRep = nullptr;

    if (pData)
    {
        pPlayer->m_teamId = pData->getTeamId();
        pPlayer->m_name = std::string(pData->getName());
        //LOG_DBG("team: %i | name: %s\n", pPlayer->m_teamId, pPlayer->m_name.c_str());

        auto pSoldier = *pData->getCharacter();
        if (pSoldier)
        {
            pPlayer->m_internalRep = (void*)pData;

            auto trans = pSoldier->getMeshTransform();
            //LOG_DBG("trans: %f\n", trans.trans.x, trans.trans.y, trans.trans.z);
            pPlayer->m_pos = D3DXVECTOR3(-trans.trans.x, trans.trans.z, trans.trans.y);
            pPlayer->m_rotation = atan2(trans.forward.z, -trans.forward.x);

            BF4::fb::Vec3 speed = { 0, 0, 0 };
            if (pData->getAttachedControllable()) {
                speed = ((BF4::ClientVehicleEntity*)pData->getAttachedControllable())->getVelocity();
                if (pSoldier->getReplicatedController())
                {
                    auto vehpos = pSoldier->getReplicatedController()->getPosition();
                    pPlayer->m_pos = D3DXVECTOR3(-vehpos.x, vehpos.z, vehpos.y);
                }
            } else if (pSoldier->getReplicatedController()) {
                speed = pSoldier->getReplicatedController()->getVelocity();
            }
            pPlayer->m_vel = D3DXVECTOR3(-speed.x, speed.z, speed.y);

            if (pSoldier->getHealthData())
                pPlayer->m_health = pSoldier->getHealthData()->getHealth() / pSoldier->getHealthData()->getMaxHealth();

            // TODO skeleton
            pPlayer->m_skeleton.m_internalRep = nullptr;

            if (isOwn) {
                // TODO weapon

                m_data.m_weapon.m_gravity = 2.0f;
                m_data.m_weapon.m_bulletSpeed = 4000.0f;
                m_data.m_weapon.m_internalRep = (void*)0x13371337;
            }
        }
    }
}
void BattlefieldDataMgr::UpdateBF4()
{
    // is ingame
    m_data.m_isIngame = true;

    // cam data
    auto pCamHolder = m_memsBF4.pCamHolder;
    if (pCamHolder)
    {
        auto pCam = pCamHolder->getCamData();
        if (pCam)
        {
            D3DXMATRIX vmat = pCam->getViewMat();
            vmat._11 = -vmat._11; // negate x
            vmat._12 = -vmat._12;
            vmat._13 = -vmat._13;
            std::swap(vmat._21, vmat._31); // swap y and z
            std::swap(vmat._22, vmat._32);
            std::swap(vmat._23, vmat._33);
            m_data.m_camData.m_viewMat = vmat;
            m_data.m_camData.m_pos = D3DXVECTOR3(-pCam->getPos().x, pCam->getPos().z, pCam->getPos().y);
            m_data.m_camData.m_viewVec = D3DXVECTOR3(pCam->getViewVec().x, -pCam->getViewVec().z, -pCam->getViewVec().y);
            m_data.m_camData.m_fovy = pCam->getFovy();
            m_data.m_camData.m_internalRep = (void*)pCamHolder;
        }
    }

    // own data
    m_data.m_ownData.m_internalRep = nullptr;

    m_data.m_weapon.m_internalRep = nullptr;

    // form context
    m_data.m_internalRep = (void*)m_memsBF4.pCtx;
    m_data.m_ownPlayerIndex = -1;
    auto ctx = m_memsBF4.pCtx;
    if (ctx->getPlayerMgr())
    {
        auto playerVec = ctx->getPlayerMgr()->getPlayers();

        m_data.m_players.resize(playerVec.size());
        for (size_t i = 0; i < playerVec.size(); i++) {
            bool isOwn = playerVec[i] == ctx->getPlayerMgr()->getLocalPlayer();
            UpdateBF4Player(&m_data.m_players[i], playerVec[i], isOwn);
            if (isOwn)
                m_data.m_ownPlayerIndex = i;
        }
    } else {
        m_data.m_players.clear();
    }
}
void __fastcall hkBF4InputUpdate(BF4::IMouse *pInst, int, float deltaTime)
{
    std::lock_guard<std::mutex> lock(g_mutexHook);

    auto bfmgr = BattlefieldDataMgr::getInstance();

    uintptr_t orgFunc = bfmgr->m_hkBF4InputUpdate->getOrgFunc(3);
    ((void(__thiscall*)(BF4::IMouse*, float))(orgFunc))(pInst, deltaTime);

    float yaw;
    float pitch;
    if (bfmgr->hkInputUpdateCam(&yaw, &pitch))
    {
        //pInst->updateLevelInCache(BFBC2::InputConceptId::Yaw, -yaw);
        //pInst->updateLevelInCache(BFBC2::InputConceptId::Pitch, pitch);
    }
    bfmgr->BF4InputCacheUpdate();
}

#include "hacklib/Rng.h"
hl::Rng g_rng;

void BattlefieldDataMgr::BF4InputCacheUpdate()
{
    auto cache = m_memsBF4.pInput->getInputCache();

    if (!m_data.getOwnPlayer() || !m_data.getOwnPlayer()->isValid())
        return;

    // spotting bot
    static hl::Timer tSpot;
    static double pauseSpot;
    if (tSpot.diff() > pauseSpot)
    {
        tSpot.reset();
        pauseSpot = g_rng.nextReal(3.0, 6.0);

        // only press the button if we are alive
        if (m_data.getOwnPlayer() && m_data.getOwnPlayer()->getHealth() > 0)
        {
            cache->m_conceptCache[BF4::InputConceptId::Spot] = 1.0f;
        }
    }

    // vehicle entry bot
    static hl::Timer tClick;
    static double pauseClick;
    if (GetAsyncKeyState(VK_NUMPAD9) < 0 && tClick.diff() > pauseClick)
    {
        tClick.reset();
        pauseClick = g_rng.nextReal(0.02, 0.05);

        cache->m_conceptCache[BF4::InputConceptId::EnterVehicle] = 1.0f;
    }

    float speed = 3.6f * D3DXVec3Length(&m_data.getOwnPlayer()->getVel());
    if (speed > 200.0f) // basic check to see if we are flying jet
    {
        static bool accel = true;
        static float bottomSpeed = 310.0f;
        static float topSpeed = 315.0f;

        if (accel)
        {
            cache->m_conceptCache[BF4::InputConceptId::MoveFB] = 1.0f;
            cache->m_conceptCache[BF4::InputConceptId::MoveForward] = 1.0f;
            if (speed > topSpeed)
            {
                topSpeed = g_rng.nextReal(313.0f, 318.0f);
                accel = false;
            }
        } else {
            cache->m_conceptCache[BF4::InputConceptId::MoveFB] = -1.0f;
            cache->m_conceptCache[BF4::InputConceptId::MoveBackward] = 1.0f;
            cache->m_conceptCache[BF4::InputConceptId::Crawl] = 1.0f;
            cache->m_conceptCache[BF4::InputConceptId::Brake] = 1.0f;
            if (speed < bottomSpeed)
            {
                bottomSpeed = g_rng.nextReal(307.0f, 312.0f);
                accel = true;
            }
        }
    }

    static bool aimbot = true;
    if (GetAsyncKeyState(VK_NUMPAD7) < 0)
    {
        aimbot = !aimbot;
    }

    // aimbot
    if (aimbot && GetAsyncKeyState(VK_CONTROL) < 0)
    {
        auto cam = m_data.getCamData();
        if (cam)
        {
            auto viewMat = cam->getViewMat();

            // transform target position to camera space. front is -z; right is x; up is y;
            D3DXVECTOR3 targetViewspace;
            D3DXVec3TransformCoord(&targetViewspace, &m_targetPos, &viewMat);

            if (targetViewspace.z < 0) // target is in front half space
            {
                // angle around right axis of camera
                float angleYZ = atan2(targetViewspace.y, -targetViewspace.z);
                // angle around up axis of camera
                float angleXZ = atan2(-targetViewspace.x, -targetViewspace.z);
                // roll towards or away from target depending on location of target in top or bottom half space
                //float yaw = 6.0f * -angleXZ;
                float pitch = 5.0f * -angleYZ;
                //float roll = 5.0f * angleXZ * -copysign(1.0f, targetViewspace.y);
                float roll = 5.0f * angleXZ;
                /*float roll;
                if (angleYZ > 1.0f)
                    roll = 3.0f * angleXZ;
                else
                    roll = 3.0f * -angleXZ;*/
                //cache->m_conceptCache[BF4::InputConceptId::Yaw] = yaw;
                cache->m_conceptCache[BF4::InputConceptId::Pitch] = pitch;
                cache->m_conceptCache[BF4::InputConceptId::Roll] = roll;
            }
        }
    }
}